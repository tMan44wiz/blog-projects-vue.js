import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'

import  BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.min.css'

import Header from './views/Header.vue';
import Footer from './views/Footer.vue';

export const eventBus = new Vue();

Vue.use(VueResource);
Vue.use(BootstrapVue);

Vue.config.productionTip = false;


Vue.component('app-header', Header);
Vue.component('app-footer', Footer);

Vue.filter("snippet", value => {
  return value.slice(0, 40) + "..."
});
Vue.filter('upperCase', value => {
  return value.toUpperCase();
});
Vue.filter('blogColor', value => {
  return value.style.color = "blue";
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
