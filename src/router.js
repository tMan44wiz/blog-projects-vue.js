import Vue from 'vue'
import Router from 'vue-router'
import BlogList from './components/BlogLists.vue'
import AddBlog from './components/AddBlog.vue'
import SingleBlog from './components/SingleBlog.vue'
import AdminLogin from './components/AdminLogin.vue'

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Blog List',
      component: BlogList,
    },

    {
      path: '/add-blog',
      name: 'Add Blog',
      component: AddBlog,
      meta: {
        require: true,
      }
    },

    {
      path: '/single-blog/:id',
      name: 'Single Blog',
      component: SingleBlog,
    },

    {
      path: '/admin-login',
      name: 'Admin Login',
      component: AdminLogin,
    }
  ]
})
